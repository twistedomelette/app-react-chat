import React, {useEffect, useState} from "react";
import "./Chat.css"
import Header from "../Header/Header";
import MessageList from "../MessageList/MessageList";
import MessageInput from "../MessageInput/MessageInput";
import Preloader from "../Preloader/Preloader";
import {Context} from "../../context";


const Chat = (props) => {
    const [items, setItems] = useState();
    const [status, setStatus] = useState("add");
    const [editId, setEditId] = useState();
    const [text, setText] = useState("");

    useEffect(() => {
        fetch(props.url)
            .then(res => res.json())
            .then(
                (result) => {
                    setItems({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    setItems({
                        isLoaded: true,
                        error
                    });
                }
            )
    }, [])

    React.useEffect(() => {
        let element = document.querySelector(".message-list");
        if (element) element.scrollTop = element.scrollHeight;
    }, [items]);

    const toggleTodo = text => {
        setItems({
            isLoaded: true,
            items: [...items.items, text]
        })
    }
    const editTodo = (data) => {
        let num;
        items.items.forEach((el, index) => {
            if (el.id === data.id) num = index;
        })
        let sItems = [...items.items];
        let fItems = [...items.items];
        sItems.splice(num);
        fItems.splice(0, num+1);
        let vItems = items.items[num];
        vItems.text = data.text;
        vItems.editedAt = `${new Date().toISOString()}`
        setItems({
            isLoaded: true,
            items: [...sItems, vItems, ...fItems]
        })
    }
    const deleteMessage = id => {
        let num;
        items.items.forEach((el, index) => {
            if (el.id === id) num = index;
        })
        let vItems = [...items.items];
        vItems.splice(num, 1)
        setItems({
            isLoaded: true,
            items: vItems
        })
    }
    const editMessage = id => {
        let num;
        items.items.forEach((el, index) => {
            if (el.id === id) num = index;
        })
        setText(items.items[num].text)
        setStatus("edit")
        setEditId(num);
    }
    return (
        <Context.Provider value={{
            toggleTodo, editTodo, deleteMessage, editMessage
        }}>
            {items ? <div className="chat">
                <Header users={items}/>
                <MessageList users={items}/>
                <MessageInput text={text} setText={setText} state={items} status={status} setStatus={setStatus} editId={editId}/>
            </div> : <Preloader/>}
        </Context.Provider>
    );
}

export default Chat;