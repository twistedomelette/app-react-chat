import React, {useContext} from "react";
import "./MessageInput.css"
import {Context} from "../../context";

const MessageInput = (props) => {
    const {toggleTodo, editTodo} = useContext(Context)

    function handleSubmit(e) {
        if (props.status === "add") {
            let Us = {
                "id": `80e00b40-1b8f-11e8-9629-${new Date().getMilliseconds()}`,
                "text": `${props.text}`,
                "userId": "80e00b40-1b8f-11e8-myId",
                "createdAt": `${new Date().toISOString()}`,
                "editedAt": "",
            }
            props.setText("");
            toggleTodo(Us);
        } else {
            editTodo({id: props.state.items[props.editId].id, text: props.text})
            props.setStatus("add");
            props.setText("");
        }
        e.preventDefault();
    }
    return (
        <div className="message-input">
            <form onSubmit={handleSubmit}>
                <div className="message-input-text">
                    <input type="text" value={props.text} onChange={e => props.setText(e.target.value)}/>
                </div>
                <div className="message-input-button">
                    <input type="submit" value="Send"/>
                </div>
            </form>
        </div>
    );
}

export default MessageInput;
