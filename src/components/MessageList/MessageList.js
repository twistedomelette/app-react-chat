import React from "react";
import "./MessageList.css"
import Message from "../Message/Message";
import OwnMessage from "../OwnMessage/OwnMessage";

const MessageList = (props) => {
    let lastDate;
    const MillisecondsInADay = 86400000;
    function newDate(item, index) {
        if (index === 0) {
            lastDate = {...item};
            return dateType(item.createdAt);
        } else if (new Date(lastDate.createdAt).getDate() !== new Date(item.createdAt).getDate()
            || new Date(item.createdAt).getTime() - new Date(lastDate.createdAt).getTime() > MillisecondsInADay) {
            lastDate = {...item};
            return dateType(item.createdAt);
        }
    }
    function dateType(item) {
        let nowDate = new Date().getDate();
        let lastDate = new Date(item).getDate();
        let value = nowDate - lastDate;
        let Milliseconds = new Date().getTime() - new Date(item).getTime();
        if (value === 0 && Milliseconds < MillisecondsInADay) {
            return "Today";
        } else if ((value === 1 || value < 0) && Milliseconds < 2 * MillisecondsInADay) {
            return "Yesterday";
        } else return new Date(item).toDateString();
    }

    function setDate(info) {
        let date;
        if (info.user.editedAt)
            date = `${new Date(info.user.editedAt).getHours()}:${new Date(info.user.editedAt).getMinutes()}`
        else
            date = `${new Date(info.user.createdAt).getHours()}:${new Date(info.user.createdAt).getMinutes()}`
        if (date) {
            let arrDate = date.split(':');
            return arrDate.map(el => {
                if (el.length !== 2)
                    return '0' + el;
                else
                    return el;
            })
        }
    }
    return (
        <div className="message-list">
            <ul>
                {props.users.items.map((item, index) => (
                    <li key={item.id}>
                        <div className="newData"><strong>{newDate(item, index)}</strong></div>
                        {item.user ? <Message user={item} setDate={setDate}/> : <OwnMessage user={item} setDate={setDate}/>}
                    </li>
                ))}
                <li><br/></li>
            </ul>
        </div>
    );
}

export default MessageList;