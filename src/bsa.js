import React from "react";
import {BrowserRouter} from "react-router-dom";
import Chat from "./components/Chat/Chat";

function App() {
  return (
      <BrowserRouter>
        <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json"/>
      </BrowserRouter>
  );
}

export default App;
