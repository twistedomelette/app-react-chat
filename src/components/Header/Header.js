import React from "react";
import "./Header.css"

const Header = (props) => {
    let date = props.users.items[props.users.items.length-1].createdAt.slice(0, 10).split('-').reverse().join('.');
    let dateFormat = new Date(props.users.items[props.users.items.length-1].createdAt);
    let time = `${dateFormat.getHours()}:${dateFormat.getMinutes()}`
    let arrDate = time.split(':');
    let correctDate = arrDate.map(el => {
        if (el.length !== 2)
            return '0'+el;
        else
            return el;
    })
    date += ` ${correctDate[0]}:${correctDate[1]}`
    let NumUsers = Participants(props.users.items)
    function Participants(users) {
        let diffUser = [];
        users.forEach(user => {
            if (!diffUser.find(el => el === user.userId)) {
                diffUser.push(user.userId);
            }
        })
        return diffUser.length;
    }
    return (
        <div className="header">
            <div className="header-title">
                My Chat
            </div>
            <div className="header-users-count">
                {NumUsers} participants
            </div>
            <div className="header-messages-count">
                {props.users.items.length} message
            </div>
            <div className="header-last-message-date">
                {date} last message
            </div>
        </div>
    );
}

export default Header;