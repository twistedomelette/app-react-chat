import React, {useState} from "react";
import "./Message.css"
import unlike from "../../icons/love.png"
import like from "../../icons/like.png"

const Message = (props) => {
    let [isLike, setIsLike] = useState(false)
    function getLike() {
        setIsLike(!isLike)
    }
    let arrData = props.setDate(props)
    return (
        <div className="message">
            <div className="message-user-avatar item">
                <img alt="avatar" src={props.user.avatar} width="55" height="55"/>
            </div>
            <div className="message-user-name item">
                <strong>{props.user.user}</strong>
            </div>
            <div className="message-text item">
                {props.user.text}
            </div>
            <div className="message-time item">
                {props.user.editedAt ? `edit ${arrData[0]}:${arrData[1]}` : `${arrData[0]}:${arrData[1]}`}
            </div>
            <div className="message-like item">
                <input type="image" src={isLike ? like: unlike} alt="like" width="25" onClick={getLike}/>
            </div>
        </div>
    );
}

export default Message;